﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Singleton class responsible for opening and closing the pause menu, start menu on world map.
/// </summary>
public class PauseManager : MonoBehaviour {

    // Singleton instance can be accessed by: PauseManager.Manager.
    public static PauseManager Manager;
    
    public GameObject PauseScreenCanvas;

    public bool Active { get; private set; }

    private CharSelectManager _charSelectScript;

    private void Awake()
    {
        Manager = this;
    }

    // Determine show canvas as active/paused when scene loads.
    void Start () {
        
        PauseScreenCanvas.SetActive(false);

        if (GameProperties.Manager.StartMenuOff)
        {
            Close();
        }
        else
        {
            Open();
        }
		
        _charSelectScript = CharSelectManager.Manager;
        
    }
	
	void Update () {
	    ScanForOpenClose("escape");
    }

    /// <summary>
    /// Open pause menu and pause game. If CharSelectManager is active, close its panel first to avoid overlap.
    /// </summary>
    public void Open()
    {
        if (Active == false)
        { 
            if (_charSelectScript != null)
            {
                _charSelectScript.Close();
            }

            Time.timeScale = 0.0F;
            PauseScreenCanvas.SetActive(true);
            Active = true;
        }
        
    }

    /// <summary>
    /// Close pause menu and resume game.
    /// </summary>
    public void Close()
    {
        if (Active)
        {
            Time.timeScale = 1.0F;
            PauseScreenCanvas.SetActive(false);
            Active = false;
        }
        
    }

    /// <summary>
    /// Scans for user keyboard input to open/close menu. 
    /// </summary>
    /// <param name="inputKey">Key to toggle pause menu. (In string format).</param>
    public void ScanForOpenClose(string inputKey)
    {
        if (Input.GetKeyDown(inputKey))
        {
            // Closing chacracter select screen.
            if (Active)
            {
                Close();
            }

            // Opening character select screen.
            else
            {
                Open();
            }
        }
    }

    /// <summary>
    /// Scans for user keyboard input to open/close menu. 
    /// </summary>
    /// <param name="inputKey">Key to toggle pause menu. (In KeyCode enum format).</param>
    public void ScanForOpenClose(KeyCode inputKey)
    {
        if (Input.GetKeyDown(inputKey))
        {
            // Closing chacracter select screen.
            if (Active)
            {
                Close();
            }

            // Opening character select screen.
            else
            {
                Open();
            }
        }
    }



}
