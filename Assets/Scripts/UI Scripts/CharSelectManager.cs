﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Script for the character selection screen. 
/// Handles the opening and closing the Character Select Screen, and calls GameManager's character switching with mouse/keyboard input.
/// </summary>

public class CharSelectManager : MonoBehaviour
{

    // Singleton instance - can be accessed by: CharSelectManager.Manager.
    public static CharSelectManager Manager;

    // If these are not provided, script will be disabled.
    public GameObject CharSelectCanvas;
    public GameObject PlayerCharacter;

    public bool Active { get; private set; }

    private GameProperties _gamePropertiesScript;
    private PlayerScript _playerScript;
    private PauseManager _pauseManagerScript;

    private int _numberOfCharacters;
    private Transform _characterButtonImage;
    private Transform _characterButtonNumber;

    /// <summary>
    /// Sets up the character select screen to display only unlocked characters. 
    /// </summary>
    public void Setup()
    {
        CharSelectCanvas.gameObject.SetActive(true);

        _gamePropertiesScript = GameProperties.Manager;
        _playerScript = PlayerCharacter.GetComponent<PlayerScript>();
        _numberOfCharacters = _gamePropertiesScript.Characters.Length;
        _pauseManagerScript = PauseManager.Manager;

        for (int i = 0; i <= _numberOfCharacters - 1; i++)
        {
            // Find UI buttons by name and set them to correspond to appropriate character for their OnClick() events.
            _characterButtonImage = CharSelectCanvas.transform.Find("CharSelect" + i);
            _characterButtonNumber = CharSelectCanvas.transform.Find("Button" + i);

            // i1 is essential for re-defining i for the scope of the lambda function. 
            // Lambda function is essential as AddListener does not accept fucntion with parameters.
            int i1 = i;
            _characterButtonImage.GetComponent<Button>().onClick.AddListener(() => {CallSwitchFucntion(i1);});
            _characterButtonNumber.GetComponent<Button>().onClick.AddListener(() => { CallSwitchFucntion(i1);});


            // Black out image if locked.
            if (_gamePropertiesScript.GetCharacterUnlocked(i) == false)
            {
                _characterButtonImage.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
                _characterButtonNumber.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
            }
        }
        CharSelectCanvas.gameObject.SetActive(false);
    }

    // Assign as singleton class.
    void Awake()
    {
        Manager = this;
    }

    // Optionality of Character select UI canvas and player. 
    void Start()
    {
        if (CharSelectCanvas == null || PlayerCharacter == null)
        {
            GetComponent<CharSelectManager>().enabled = false;
        }
        else
        {
            Setup();
        }

    }

    void Update()
    {
        ScanForOpenClose("tab");
        KeyboardSelectCharacter();
    }

    /// <summary>
    /// Closes character select screen.
    /// </summary>
    public void Close()
    {
        if (Active)
        {
            CharSelectCanvas.gameObject.SetActive(false);
            Active = false;
            Time.timeScale = 1.0F;
        }
    }

    /// <summary>
    /// Opens character select screen. 
    /// </summary>
    public void Open()
    {
        if (Active == false)
        {
            if (_pauseManagerScript.Active) return;
            CharSelectCanvas.gameObject.SetActive(true);
            Active = true;
            Time.timeScale = 0.05F;
        }
    }
    
    /// <summary>
    /// Scans for user keyboard input to open character select screen. 
    /// </summary>
    /// <param name="inputKey"> String described input key. See Unity Conventional Game Input docs. </param>
    public void ScanForOpenClose(string inputKey)
    {
        if (Input.GetKeyDown(inputKey))
        {
            // Closing chacracter select screen.
            if (Active)
            {
                Close();
            }

            // Opening character select screen.
            else
            {
                Open();
            }
        }
    }

    /// <summary>
    /// Scans for user keyboard input to open character select screen. 
    /// </summary>
    /// <param name="inputKey"> Keycode enum described input key. See Unity KeyCode docs.  </param>
    public void ScanForOpenClose(KeyCode inputKey)
    {
        if (Input.GetKeyDown(inputKey))
        {
            // Closing chacracter select screen.
            if (Active)
            {
                Close();
            }

            // Opening character select screen.
            else
            {
                Open();
            }
        }
    }

    /// <summary>
    /// Select character using keyboard number keys.
    /// </summary>
    public void KeyboardSelectCharacter()
    {
        for (int i = 0; i <= _numberOfCharacters - 1; i++)
        {
            if (Input.GetKeyDown((i + 1).ToString()))
            {
                CallSwitchFucntion(i);
            }
        }
    }

    /// <summary>
    /// Calls the player's character switching funtion if the player is unlocked according to GameProperties.
    /// </summary>
    /// <param name="characterNumber"> Character to switch to. Indexed at 0.</param>
    public bool CallSwitchFucntion(int characterNumber)
    {
        if (_gamePropertiesScript.GetCharacterUnlocked(characterNumber))
        {
            _playerScript.Switch(characterNumber);
            return true;
        }
        return false;
    }

}