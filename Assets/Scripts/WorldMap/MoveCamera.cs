﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows movement in world map using click and drag.
/// </summary>
public class MoveCamera : MonoBehaviour 
{
    // Speed of the camera when being panned
    public float PanSpeed;

    // Position of cursor when mouse dragging starts
    private Vector3 _mouseOrigin;

    private bool _isPanning;

	void Update () 
	{
		// Get the left mouse button
		if(Input.GetMouseButtonDown(0))
		{
			// Get mouse origin
			_mouseOrigin = Input.mousePosition;
			_isPanning = true;
		}

		// Disable movements on button release
		if (!Input.GetMouseButton(0)) _isPanning=false;

		// Move the camera on it's XY plane
		if (_isPanning)
		{
			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _mouseOrigin);

			Vector3 move = new Vector3(pos.x * PanSpeed, pos.y * PanSpeed, 0);
			transform.Translate(move, Space.Self);
		}
	}
}