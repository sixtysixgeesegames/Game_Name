﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float Speed = 1;

    public BoxCollider2D BoundBox;
    private Vector3 _minBounds;
    private Vector3 _maxBounds;

    private Camera _theCamera;
    private float _halfHeight;
    private float _halfWidth;


    private void Start()
    {
        _minBounds = BoundBox.bounds.min;
        _maxBounds = BoundBox.bounds.max;

        _theCamera = GetComponent<Camera>();
        _halfHeight = _theCamera.orthographicSize;
        _halfWidth = _theCamera.orthographicSize * Screen.width / Screen.height;

    }
    void Update()
    {
        float xAxisValue = Input.GetAxis("Horizontal") * Speed;
        float yAxisValue = Input.GetAxis("Vertical") * Speed;

        transform.position = new Vector3(transform.position.x + (xAxisValue/100), transform.position.y + (yAxisValue/100), transform.position.z);

        float clampedX = Mathf.Clamp(transform.position.x, _minBounds.x + _halfWidth, _maxBounds.x - _halfWidth);
        float clampedY = Mathf.Clamp(transform.position.y, _minBounds.y + _halfHeight, _maxBounds.y - _halfHeight);

        transform.position = new Vector3(clampedX, clampedY, transform.position.z);
    }
}

