﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WorldMap : MonoBehaviour
{
    public GameObject WorldMapCanvas;

    private GameProperties _gamePropertiesScript;
    private int _numberOfLevels;
    private Transform _levelButton;
    private Transform _levelText;

    private LevelManager _levelManagerScript;

    // Sets up map by adding on click events according to levels unlocked.
    void Start () {
        _gamePropertiesScript = GameProperties.Manager;
	    _numberOfLevels = _gamePropertiesScript.Levels.Length;
        _levelManagerScript = LevelManager.Manager;

        for (int i = 0; i <= _numberOfLevels - 1; i++)
        {
            // Find button (map node) and text associated with each level.
            _levelButton = WorldMapCanvas.transform.Find("ButtonLvl" + i);
            _levelText = WorldMapCanvas.transform.Find("TextLvl" + i);

            if (_gamePropertiesScript.GetLevelUnlocked(i))
            {
                // Lambda function needed for AddListener, which only accepts fucntions with no paramaters. i1 needed to redefine i in lambda's scope.
                int i1 = i;
                _levelButton.GetComponent<Button>().onClick.AddListener(() => { LoadLevel(i1); });
                _levelText.GetComponent<Button>().onClick.AddListener(() => { LoadLevel(i1); });
            }

            // If level is locked buttons cannot be clicked and text for level isn't visible. 
            else
            {
                _levelButton.GetComponent<Button>().interactable = false;
                _levelText.gameObject.SetActive(false);
            }
        }
	}

    /// <summary>
    /// Private function to call Manager's LoadLevel. Added as listeners in Start.
    /// </summary>
    /// <param name="levelNumber">Level to be loaded.</param>
    private void LoadLevel(int levelNumber)
    {
        _levelManagerScript.LoadLevel("level" + levelNumber);
    }
}
