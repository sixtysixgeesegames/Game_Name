﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : MonoBehaviour {

    public GameObject AttackIndicator;

    private float[] _alarms = new float[12]; //set a series of alarms that can be used for delays and timings 
    private bool _attacking = false; // currently charging an attack?
    private int _attackType; // 1 = dodge, 2 = block
    private int _attackdmg = 0; // how much damage to do
    public int[] AttackPossibility; // the type of attacks this enemy can use
    public int AttackSpeed; // how long to stay in the charge state
    public int FireRate; //how long to wait between attacks
    private int _attack; // attack to do 
    private int _state = 0; // 0 = idle, 1 = player detect, 2 = wanting to attack, 3 = attacking 

    private GameObject _player;
    private LayerMask _layerMask;
    private Animator _anim;




    // Use this for initialization
    /// <summary>
    /// Retrieves all the components that are available and initialise alarms.
    /// </summary>
    void Start () {
        _player = GameObject.FindGameObjectWithTag("Player");
        _layerMask =  LayerMask.GetMask("Player");
        AttackIndicator.GetComponent<SpriteRenderer>().enabled=false;
        _anim = GetComponent<Animator>();

        for (int i = 0; i < _alarms.Length; i++)
        {
            _alarms[i] = -1;
        }

        }

    // Update is called once per frame
    void FixedUpdate() {
        //alarm controll
        for (int i = 0; i < _alarms.Length; i++)
        {

            if (_alarms[i] > 0)
                _alarms[i] -= 1;
            if (_alarms[i] == 0)
            {
                _alarms[i] = -1;
                //alarm when triggered
                switch (i)
                {
                    //deal damage state
                    case 0:
                        _state = 3;
                        _alarms[1] = FireRate; 
                        
                        break;
                    //stop attacking
                    case 1:
                        _attacking = false;
                        break;
                }

                

            }
        }
        //calculate the distance to the player object from this position
        var distToPlayer = _player.transform.position - transform.position;
        //large switch statement based on the state of the ai to controll how it behaves, think of it as an FSA.
        switch (_state)
        {
            //idle
            case 0:
                //draw a ray of length  GetComponent<BoxCollider2D>().size.x*2 if it hits the player first i.e can see the player go to follow state else stay still 
                var rayDirection = _player.transform.position - transform.position;
                var hit = Physics2D.Raycast(transform.position, rayDirection, GetComponent<BoxCollider2D>().size.x*2, _layerMask);
                GetComponent<SimpleEnemyMovent>().SetMove(false);
                if (hit.collider != null)
                {
                    if (hit.collider.tag == "Player")
                    {
                        GetComponent<SimpleEnemyMovent>().SetMove(true);
                        _state = 1;
                        

                    }
                }
                break;
            //follow
            case 1:
                //follow the player
                FollowPlayer(distToPlayer);
                //if close enough to the player and can attack start charging an attack
                if (Mathf.Abs(distToPlayer.x) <= GetComponent<BoxCollider2D>().size.x && _attacking == false)
                {

                    _attack = AttackPossibility[(Random.Range(0, AttackPossibility.Length))]; //pick an attack
                    _state = 2; // go to charging state
                }
                // if too far away from the player i.e got away stop following and go idle.
                else if (Mathf.Abs(distToPlayer.x) >= GetComponent<BoxCollider2D>().size.x*3 || Mathf.Abs(distToPlayer.y) >= GetComponent<BoxCollider2D>().size.y+2)
                {
                    _state = 0;
                }
                break;
            //charging
            case 2:
                FollowPlayer(distToPlayer); //follow the player still

                //player response i.e dodge or counter + enemy attack image + set alarm to 0
                if (_attacking == true)
                {
                    // if attacking and the player is out of range stop attacking
                    if (Mathf.Abs(distToPlayer.x) >= GetComponent<BoxCollider2D>().size.x ||  Mathf.Abs(distToPlayer.y) >= GetComponent<BoxCollider2D>().size.y)
                    {
                        _attack = 0;
                        CancleAttack();
                    }
                    //if attack type is parry set animation and charge check to see if the player counters the attack or makes a mistake.
                    else if (_attackType == 1)
                    {
                        if (_attack == 1)
                            _anim.SetTrigger("Parry1");
                        else
                            _anim.SetTrigger("Parry2");
                        //correct input enemy takes damage from parry 
                        if (Input.GetKeyDown(KeyCode.UpArrow))
                        {
                            
                            CancleAttack();
                            GetComponent<Rigidbody2D>().AddForce(new Vector2(distToPlayer.x / Mathf.Abs(distToPlayer.x) * -500, 300));
                            GetComponent<EnemyHealth>().TakeDamage(_player.GetComponent<PlayerScript>().GetAttack());
                        }
                        //player made a mistake player takes dammage
                        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
                        {
                            _player.GetComponent<PlayerScript>().Takedammage(1);
                            CancleAttack();
                            _player.GetComponent<Rigidbody2D>().AddForce(new Vector2(distToPlayer.x / Mathf.Abs(distToPlayer.x) * 800, 400));

                        }



                    }
                    //if attack type is evade set animation and charge check to see if the player counters the attack or makes a mistake.
                    else if (_attackType == 2 )
                    {
                        _anim.SetTrigger("Evade1");
                        //player dodges 
                        if (Input.GetKeyDown(KeyCode.DownArrow))
                        {    
                            CancleAttack();
                        }
                        //player makes a mistake
                        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
                        {
                            _player.GetComponent<PlayerScript>().Takedammage(2);
                            CancleAttack();
                            _player.GetComponent<Rigidbody2D>().AddForce(new Vector2(distToPlayer.x / Mathf.Abs(distToPlayer.x) * 800, 400));

                        }
                    }
                }
                //if not attacking look at what the attack is and decide how much dammage and what type it is.
                else if (_attacking == false)
                {
                    if (_attack%2==1)
                    {
                        _attackType = 1;
                        _attackdmg = 1;
                    }
                    else
                    {
                        _attackType = 2;
                        _attackdmg = 2;
                    }
                    //set the approriate sprite for the attack 
                    AttackIndicator.GetComponent<SpriteRenderer>().enabled = true;
                    if (_attackType == 2)
                        AttackIndicator.GetComponent<AttackIndicator>().Block();
                    else
                        AttackIndicator.GetComponent<AttackIndicator>().Dodge();
                    //set how long until the damage takes place
                    _alarms[0] = AttackSpeed; //attackspeed
                    _attacking = true;
                }
                break;
            //attacking
            //deal damage
            case 3:

                //disable attack sprite

                AttackIndicator.GetComponent<SpriteRenderer>().enabled = false;
                if (_attacking)
                {
                    ClearAnimTriggers();
                    
                    //deal knockback and dammage to player
                    _player.GetComponent<Rigidbody2D>().AddForce (new Vector2(distToPlayer.x / Mathf.Abs(distToPlayer.x) * 500, 300));
                    _player.GetComponent<PlayerScript>().Takedammage(_attackdmg);
                    
                }
                //chain attacks
                if (_attack >2)
                {
                    if (_attack == 3)
                        _attack = 2;
                    else if(_attack==4)
                        _attack = 2;
                    else if (_attack == 5)
                        _attack = 1;
                    else if (_attack == 6)
                        _attack = 1;
                    else if (_attack == 7)
                        _attack = 3;
                    else if (_attack == 8)
                        _attack = 3;
                    else if (_attack == 9)
                        _attack = 5;
                    else if (_attack == 10)
                        _attack = 5;
                    else if (_attack == 11)
                        _attack = 4;
                    else if (_attack == 12)
                        _attack = 4;
                    else if (_attack == 13)
                        _attack = 6;
                    else if (_attack == 14)
                        _attack = 6;
                    //go back to charging
                    _state = 2;
                    _attacking = false;
                    _alarms[1] = -1;
                    _alarms[0] = -1;
                }
                else
                {
                    _state = 1;
                    Debug.Log("State=1");
                }
                
                
                break;
        }

    }
    /// <summary>
    /// Cancles the attack starts the next attack if there is one
    /// </summary>
    public void CancleAttack()
    {
        ClearAnimTriggers();
        AttackIndicator.GetComponent<SpriteRenderer>().enabled = false;
        _alarms[0] = -1;
        if (_attack>2)
        {
            _alarms[1] = FireRate;
            _state = 3;
            Debug.Log("State=3");
            _attacking = false;
            
        }
        else
        { 
            _alarms[1] = FireRate;

            _state = 1;
        }
        
    }
    /// <summary>
    /// Follows the player and stops the ai from walking in to somewhere
    /// </summary>
    /// <param name="distToPlayer"> the distance to player using vector3</param>
    private void FollowPlayer(Vector3 distToPlayer)
    {
        GetComponent<EnemyHealth>().UpdateHealthBar();
        if (Mathf.Abs(distToPlayer.x) > GetComponent<BoxCollider2D>().size.x/2+0.1)
        {
            GetComponent<SimpleEnemyMovent>().movetowardsplayer(_player);
            GetComponent<SimpleEnemyMovent>().SetMove(true);
        }
      else
            GetComponent<SimpleEnemyMovent>().SetMove(false);



    }
    /// <summary>
    /// clears all the animation
    /// </summary>
    private void ClearAnimTriggers()
    {
        _anim.ResetTrigger("Evade1");
        _anim.ResetTrigger("Parry1");
        _anim.ResetTrigger("Parry2");
    }
}
