﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackIndicator : MonoBehaviour {
    
    public Sprite[] Sprites; // collection of sprites to represent the next attack the enemy indicates to do.

    //Sprites[0]=dodge
    //Sprites[1]=parry

    /// <summary>
    /// Sets the image to be that of the one that implies dodge.
    /// </summary>
    public void Dodge()
    {
        GetComponent<SpriteRenderer>().sprite = Sprites[0];
    }
    /// <summary>
    /// Sets the image to be that of the one that implies parry.
    /// </summary>
    public void Block()
    {
        GetComponent<SpriteRenderer>().sprite = Sprites[1];
    }

}
