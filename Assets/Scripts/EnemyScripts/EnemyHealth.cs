﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

    public float StartingHealth;
    public GameObject HealthCanvas;
    private float _health ;
    private Animator _anim;


    void Start () {

        _health = StartingHealth;
        _anim = GetComponent<Animator>();
    }
    /// <summary>
    /// returs the current health of the object
    /// </summary>
    /// <returns>return health as a float</returns>
    public float GetHealth()
    {
        return _health;
    }
    /// <summary>
    /// update healthbar to reflect dammage
    /// </summary>
    public void UpdateHealthBar()
    {
        if (HealthCanvas != null)
        {
            HealthCanvas.transform.Find("EnemyHealthBar").GetComponent<Image>().fillAmount =
                (float)GetComponent<EnemyHealth>().GetHealth() / (float)GetComponent<EnemyHealth>().StartingHealth;
            HealthCanvas.transform.Find("EnemyName").GetComponent<Text>().text = name;
        }

    }
    /// <summary>
    /// take damage 
    /// </summary>
    /// <param name="amount">float amount to take dammage byy</param>

    public void TakeDamage(float amount)
    {
        _health -= amount;
        if (_health <= 0)
        {
            _anim.SetTrigger("Death");
            GetComponent<EnemyCombat>().CancleAttack();
        }
    }
    /// <summary>
    /// destroy game object
    /// </summary>
    public void Death()
    {
        Destroy(gameObject);
        
    }
}
