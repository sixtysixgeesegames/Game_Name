﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemyMovent : MonoBehaviour {

    public LayerMask WhatIsGround;
    public Transform GroundCheck;
    public float HorizontalSpeed;
    public float MaxSpeed;

    private bool _grounded = false;
    private float _groundRadius = 0.8f;
    private bool move;
    private Animator anim;
   

    // Use this for initialization
    void Start () {
        //set and get components
        anim = GetComponent<Animator>();
        GetComponent<Rigidbody2D>().drag = 0;
        move = false;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //if can move move in direction of facing unless it would fall down a hole then flip
        _grounded = Physics2D.OverlapCircle(new Vector2(GroundCheck.position.x,GroundCheck.position.y), _groundRadius, WhatIsGround);
        anim.SetBool("Ground", _grounded);
        if (_grounded)
        {
            anim.SetFloat("Hspeed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x)/2);
            if ((GetComponent<Rigidbody2D>().velocity.magnitude < MaxSpeed) && move)
            {
                GetComponent<Rigidbody2D>().velocity += (new Vector2((transform.localScale.x/Mathf.Abs(transform.localScale.x)) * HorizontalSpeed, 0));
                
            }
            
        }
        else
        {
            Flip();
        }
        if (!Physics2D.OverlapCircle(new Vector2(GroundCheck.position.x+transform.localScale.x, GroundCheck.position.y), _groundRadius, WhatIsGround))
        {
            Flip();
        }

    }
    //if in the player stop moving
    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
            move = false;
    }
    //if leaving the player move
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
            move = true;
    }
    /// <summary>
    /// Flip the sprite
    /// </summary>
    private void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
    /// <summary>
    /// movetowards the player
    /// </summary>
    /// <param name="player"> player game object</param>
    public void movetowardsplayer(GameObject player)
    {
        
        if((transform.position.x < player.transform.position.x && transform.localScale.x<0)||(transform.position.x > player.transform.position.x && transform.localScale.x > 0) )
        {
            Flip();
        }
    }
    /// <summary>
    /// set move to move
    /// </summary>
    /// <param name="mov"> bool true to move</param>
    public void SetMove(bool mov)
    {
        move = mov;
        if (mov == true)
        {
            anim.SetTrigger("Move");
        }
        else
        {
            anim.SetFloat("Hspeed", 0);
            anim.SetTrigger("Crouch");
        }
    }
}
