﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    public float YVelocity;
    public float XVelocity;
    private bool _move=false;
	
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 pos = transform.position;
        //if can move then update move
        if (_move)
        {
            pos.x += XVelocity;
            pos.y += YVelocity;
            transform.position = pos;
        }
		
	}

    private void OnCollisionStay2D(Collision2D other)
    {
        //move the player when player in me 
        if(other.gameObject.tag=="Player" && _move)
        {
            other.transform.position = new Vector3(other.transform.position.x + XVelocity, other.transform.position.y + YVelocity,other.transform.position.z);
        }
    }

   

    private void OnTriggerStay2D(Collider2D other)
        //stop when hitting movplataid
    {
        if (other.gameObject.tag == "MovPlatAid"){
            transform.position = new Vector2(transform.position.x - XVelocity, transform.position.y - YVelocity);
            _move = false; }
    }

    /// <summary>
    /// Swap the direction
    /// </summary>
    public void SwapDirection()
    {
        _move = true;
        XVelocity = XVelocity * -1;
        YVelocity = YVelocity * -1;
    }
}
