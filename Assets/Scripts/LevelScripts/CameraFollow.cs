﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

   public GameObject ObjToFollow; //usually the character gameobject
    public float StepSize; //0.3 is good
    public Vector3 Offset; //0 15 -10 is good but tweak as see fit
    private Vector3 _velocity = Vector3.zero;

	
	// Update is called once per frame
	void Update () {
        Vector3 _targetPosition = ObjToFollow.transform.TransformPoint(Offset);
        //smooth camera follow function
        transform.position = Vector3.SmoothDamp(transform.position, _targetPosition, ref _velocity, StepSize);

    }
}
