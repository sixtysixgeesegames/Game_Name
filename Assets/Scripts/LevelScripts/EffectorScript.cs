﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectorScript : MonoBehaviour {
    public int Channel;
    public int Effect; //0 destroys instance 1=swap direction if eleveator

    /// <summary>
    /// triggers the efect
    /// </summary>
    public void Action()
    {
        switch (Effect) {
            case 0:
                Destroy(gameObject);
                break;
            case 1:
                if(GetComponent<MovingPlatform>())
                {
                    GetComponent<MovingPlatform>().SwapDirection();
                }
                break;
    }
    }
}
