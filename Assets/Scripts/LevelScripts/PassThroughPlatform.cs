﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassThroughPlatform : MonoBehaviour {


     public float DownValue;
     public float UpValue;
     private bool _goUp;

    /// <summary>
    /// Lets the layer pass through
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            other.gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
        }
        if (other.gameObject.tag == "Player" && Input.GetKey(KeyCode.W) && _goUp)
            other.gameObject.transform.position += new Vector3(0, UpValue);
        if (other.gameObject.tag == "Player")
            _goUp = true;
        else
            _goUp = false;
    }
    /// <summary>
    /// become solid when the player leaves
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
            other.gameObject.GetComponent<CircleCollider2D>().isTrigger = false;
        }
        _goUp = false;
    }
    /// <summary>
    /// force player up
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionStay2D(Collision2D other)
    {
    
        if (other.gameObject.tag == "Player" && Input.GetKey(KeyCode.S))
            other.gameObject.transform.position += new Vector3(0, DownValue);

        if (other.gameObject.tag =="Player" && Input.GetKey(KeyCode.S))
        {
            other.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            other.gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
        }
    }
}
