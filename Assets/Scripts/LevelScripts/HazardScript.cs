﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardScript : MonoBehaviour {
    //deal dammage if the player is above the hazard and hits it


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (other.GetComponent<Rigidbody2D>().velocity.y < -2)
            {
                other.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 800));
                other.GetComponent<PlayerScript>().Takedammage(1);
            }
        }
    }
}
