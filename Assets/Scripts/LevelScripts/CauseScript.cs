﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CauseScript : MonoBehaviour {

    public int Channel;
    private GameObject[] _effectors;
    private bool _switched = false;



    // Update is called once per frame
    private void OnTriggerStay2D(Collider2D other)
    {
        //if the player presees the interaction flip and trigger efects to hapen
        if (other.gameObject.tag=="Player"&& Input.GetKeyDown(KeyCode.S)&&!_switched)
        {
            _switched = true;
            Flip();

            _effectors = GameObject.FindGameObjectsWithTag("Effect");

            foreach (GameObject effector in _effectors)
            {
                if(effector.GetComponent<EffectorScript>().Channel == Channel)
                    effector.GetComponent<EffectorScript>().Action();
            }
        }
    }
    /// <summary>
    /// flip the image
    /// </summary>
    private void Flip()
    {
        Vector3 thescale = transform.localScale;
        thescale.x = thescale.x * -1;
        transform.localScale = thescale;
    }

}

