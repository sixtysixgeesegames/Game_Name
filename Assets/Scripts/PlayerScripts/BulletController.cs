﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    
    public bool Fire = true;
    public Sprite[] Sprites;

    private Vector2 _v;
    private bool _col = false;
    private int _damage;
   

    // Use this for initialization
    void Start()
    {
       //move bullet then set angle based on movement
        Vector2 moveDirection = GetComponent<Rigidbody2D>().velocity;
        if (moveDirection != Vector2.zero)
        {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


        }
    }

    // Update is called once per frame
   

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (_col == false)
        {
            //if the object can be colldied with stop else if enemy do dammage else pass right on through
            if (collision.gameObject.tag != "Player" && collision.gameObject.tag!="NoCollide")
            {
                _col = true;
                GetComponent<Rigidbody2D>().gravityScale = 1;
            }
            if (collision.gameObject.tag == "Enemy")
            {
                EnemyHealth enemyHealth = collision.gameObject.GetComponent<EnemyHealth>();
                enemyHealth.TakeDamage(_damage);
                enemyHealth.UpdateHealthBar();
            }
        }
    }
    /// <summary>
    /// set dammage
    /// </summary>
    /// <param name="val"> the amount of damage int</param>
    public void SetDammage(int val)
    {
        _damage = val;
    }
    /// <summary>
    /// set the image to be a bullet
    /// </summary>
    public void Bullet()
    {
        GetComponent<SpriteRenderer>().sprite = Sprites[0];
    }
    /// <summary>
    /// set the image to be a greneade 
    /// </summary>
    public void Grenade()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0.1F;
        GetComponent<SpriteRenderer>().sprite = Sprites[1];
    }
    /// <summary>
    /// set the image to be a rocket
    /// </summary>
    public void Rocket()
    {
        GetComponent<SpriteRenderer>().sprite = Sprites[2];
    }
    /// <summary>
    /// set the image to be a knife.
    /// </summary>
    public void Knife()
    {
        GetComponent<SpriteRenderer>().sprite = Sprites[3];
        GetComponent<Rigidbody2D>().gravityScale = 0.05F;
    }


}



