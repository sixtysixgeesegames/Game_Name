﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatTip : MonoBehaviour {

    private bool _attack = false;
    private float _xp;
    private float _yp;
    private int _dam;

   
    private void OnTriggerStay2D(Collider2D other)
    {
        //when hit enemy and attacking deal damage
        if (other.gameObject.tag == "Enemy" && _attack)
        {

            EnemyHealth enemyHealth = other.gameObject.GetComponent<EnemyHealth>();
            enemyHealth.TakeDamage(_dam);
            
            PlayerScript player = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<PlayerScript>();
            player.CanAttack = false;
            player.IncreaseEnergy(2);
            _attack = false;
            other.GetComponent<Rigidbody2D>().AddForce(new Vector2(( player.GetFacing())*600, 400));
        }
    }
    /// <summary>
    /// set damage from player
    /// </summary>
    /// <param name="val"> the amount of damage to do int</param>
    public void SetDammage(int val)
    {
        _dam = val;
    }
    /// <summary>
    /// set can attack to enable damage
    /// </summary>
    /// <param name="val">bool true to do dammage false otherwise</param>
    public void SetAttack(bool val)
    {
        _attack = val;
    }
    /// <summary>
    /// set xp to move in x direction
    /// </summary>
    /// <param name="val">float amount 0.4 is good</param>
    public void Setxp(float val)
    {
        _xp = val;
    }
    /// <summary>
    /// set y to move in y direction
    /// </summary>
    /// <param name="val">float amount 0.4 is good</param>
    public void Setyp(float val)
    {
        _yp = val;
    }
}
