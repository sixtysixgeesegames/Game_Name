﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {


    private Vector2 _velocityVector;
    private float[] _alarms = new float[12];// alarms for triggering delays
    
    
    private bool _facingRight = true; //facing right?
   

    // Ground check/collisions
    public Transform GroundCheck;
    public LayerMask WhatIsGround;
    private bool _grounded;
    private int _state;
    private float _groundRadius = 0.2f;
    private bool _canCollideWithRope = true;

    public GameObject HealthBarCanvas;

    // Movement related 
    public float JumpForce ;
    private float _spd = 0;
    private float _maxspd = 7f;
    private float _acc = 0.2f;
    private float _dacc = 0.8f;
    private Vector3 _climbPos;
    private bool _canMove = true;

   
    private double _energy;
    private double[] _energyCost = new double[]{ 4, 2, 8, 5, 3, 1 };

    // Combat related
    public GameObject Projectile;
    public Transform CombatPoint;
    public bool CanAttack = true;
    public int MeleeDamage;
    public int RangedDamage;

    // Health related
   
    private float _health;
    private bool _invulnerable;

    private Vector3 _checkpoint;
    private Vector3 _scale;
    public Transform Check;

    // Animation
    private Animator _anim;
    private int _character;
    public RuntimeAnimatorController CompSci;
    public RuntimeAnimatorController Bio;
    public RuntimeAnimatorController Phy;
    public RuntimeAnimatorController Chem;
    public RuntimeAnimatorController Art;
    public RuntimeAnimatorController TV;

    private GameProperties _gamePropertiesScript;


    // Use this for initialization
    void Start ()
    {
        _state = 0; //0 idle 1 walking 2 jumping/falling 3 edging 4 climbing 5 rope
        _gamePropertiesScript = GameProperties.Manager;

        _anim = GetComponent<Animator>();
        GetComponent<Rigidbody2D>().drag = 0;
        //set variables
        _health = GameProperties.Manager.PlayerMaxHealth;
        _energy = GameProperties.Manager.PlayerMaxEnergy;
        _checkpoint = transform.position;
        _scale = transform.localScale;
        _character = GameProperties.Manager.GetLastPlayedCharacter(); //get last character played 
        Switch(_character);
        CombatPoint.GetComponent<CombatTip>().SetDammage(MeleeDamage);
       

    }

    void Update()
    {
        _velocityVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        
        //jumping
        if (_grounded && Input.GetKeyDown(KeyCode.W))
        {
            _state = 2;
            _anim.SetBool("Ground", false);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, JumpForce));

        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //alarms
        for(int i=0;i<_alarms.Length;i++)
        {
            
            if (_alarms[i] > 0)
                _alarms[i] -= 1;
            if (_alarms[i]==0)
                switch (i)
                {
                    case 0:
                        _invulnerable = false;
                        break;
                    case 1:
                        _canCollideWithRope = true;
                        break;
                }
        }
        //check grounded
        _grounded = Physics2D.OverlapCircle(GroundCheck.position, _groundRadius, WhatIsGround);
        if (_grounded == true && _state == 2)
            _state = 0;
        _anim.SetBool("Ground", _grounded);
        _anim.SetBool("Fire", false);

        _anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);

        

        if (_canMove == true)
        {
            if (_grounded)
            {
                //shooting 
                if (CanAttack && Input.GetKeyDown(KeyCode.LeftShift) && _energy>=_energyCost[_character])
                {
                    _anim.SetBool("Fire", true);
                    CanAttack = false;
                    

                }
                //attacking leftarrow and right arrow
                else if (CanAttack && (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)))
                {
                    if (Input.GetKeyDown(KeyCode.LeftArrow) && _facingRight)
                        Flip();
                    else if (Input.GetKeyDown(KeyCode.RightArrow) && !_facingRight)
                        Flip();
                    CombatPoint.gameObject.GetComponent<CombatTip>().SetAttack(CanAttack);
                    CombatPoint.gameObject.GetComponent<CombatTip>().Setxp(transform.position.x + transform.localScale.x * (float)10);
                    CombatPoint.gameObject.GetComponent<CombatTip>().Setyp(transform.position.y+1);
                    _anim.ResetTrigger("Thrustu");
                    _anim.ResetTrigger("Thrustd");
                    _anim.SetTrigger("Thrustf");
                    



                }
                //parry
                else if (CanAttack && (Input.GetKeyDown(KeyCode.UpArrow)))
                {
                    CombatPoint.gameObject.GetComponent<CombatTip>().SetAttack(false);
                    CombatPoint.gameObject.GetComponent<CombatTip>().Setxp(transform.position.x + transform.localScale.x * (float)2);
                    CombatPoint.gameObject.GetComponent<CombatTip>().Setyp(transform.position.y + (float)3);
                    _anim.ResetTrigger("Thrustf");
                    _anim.ResetTrigger("Thrustd");
                    _anim.SetTrigger("Thrustu");


                }
                //evade
                else if (CanAttack && (Input.GetKeyDown(KeyCode.DownArrow)))
                {
                    CombatPoint.gameObject.GetComponent<CombatTip>().SetAttack(false);
                    CombatPoint.gameObject.GetComponent<CombatTip>().Setxp(transform.position.x + transform.localScale.x * (float)2);
                    CombatPoint.gameObject.GetComponent<CombatTip>().Setyp(transform.position.y - (float)3);
                    _anim.ResetTrigger("Thrustu");
                    _anim.ResetTrigger("Thrustf");
                    _anim.SetTrigger("Thrustd");


                }
                //update position
                if (_velocityVector.x < -0.4 && _spd > -_maxspd)
                {
                    _spd = _spd - _acc;
                    _state = 1;
                }
                else if (_velocityVector.x > 0.4 && _spd < _maxspd)
                {
                    _state = 1;
                    _spd = _spd + _acc;
                }
                else
                {
                    if (_spd > _dacc)
                        _spd = _spd - _dacc;
                    else if (_spd < -_dacc)
                        _spd = _spd + _dacc;
                    else
                        _spd = 0;
                }

               


            }
            else
            {
                if (_velocityVector.x < -0.4 && _spd > -_maxspd)
                    _spd = _spd - _acc / 2;
                else if (_velocityVector.x > 0.4 && _spd < _maxspd)
                    _spd = _spd + _acc / 2;
                else
                {
                    if (_spd > _dacc / 2)
                        _spd = _spd - _dacc / 2;
                    else if (_spd < -_dacc / 2)
                        _spd = _spd + _dacc / 2;
                    else
                        _spd = 0;
                }
            }
            if (_velocityVector.x > 0 && !_facingRight)
                Flip();
            else if (_velocityVector.x < 0 && _facingRight)
                Flip();
        }
        else
        {
            _spd = 0;
            //climb up
            if (_anim.GetBool("Edging") && Input.GetKeyDown(KeyCode.W))
                {
                _state = 4;

                _anim.SetBool("RopeClimb", false);
                _anim.SetBool("Climbing", true);
                _anim.SetBool("Edging", false);
            }
            //fall down
            if (_anim.GetBool("Edging") && Input.GetKeyDown(KeyCode.S))
            {
                _state = 2;

                _anim.SetBool("Edging", false);
                GetComponent<Rigidbody2D>().gravityScale = 1;
                _canMove = true;
            }
            //rope climbing stuff
            if (_state == 5)
            {
                _anim.SetFloat("vSpeed", 0);
                if (Input.GetKey(KeyCode.W))
                {
                    GetComponent<Rigidbody2D>().MovePosition(transform.position + new Vector3(0, (float)+0.1, 0));
                    _anim.SetFloat("vSpeed", 3);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    GetComponent<Rigidbody2D>().MovePosition(transform.position + new Vector3(0, (float)-0.1, 0));
                    _anim.SetFloat("vSpeed", -3);
                }

                if (Input.GetKeyDown(KeyCode.D) && !_facingRight)
                {
                    _canCollideWithRope = false;
                    _state = 2;
                    _canMove = true;
                    GetComponent<Rigidbody2D>().gravityScale = 1;
                    _anim.SetBool("RopeClimb", false);
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(700, 500));
                    _alarms[1] = 40;

                }

                if (Input.GetKeyDown(KeyCode.A) && _facingRight)
                {
                    _canCollideWithRope = false;
                    _state = 2;
                    _canMove = true;
                    GetComponent<Rigidbody2D>().gravityScale = 1;
                    _anim.SetBool("RopeClimb", false);
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(-700, 500));
                    _alarms[1] = 40;

                }

                if (Input.GetKey(KeyCode.D) && _facingRight)
                {
                    _facingRight = true;
                    Flip();

                }
                if (Input.GetKey(KeyCode.A) && !_facingRight)
                {
                    _facingRight = false;
                    Flip();

                }
            }
        }
        GetComponent<Rigidbody2D>().velocity=new Vector2(_spd, GetComponent<Rigidbody2D>().velocity.y);


        _anim.SetFloat("Speed", Mathf.Abs(_spd));
        


    
		
	}
    
  
    private void OnCollisionEnter2D(Collision2D other)
        //collectibles here
    {
        if (other.gameObject.tag=="Collectable")
        {
            Destroy(other.gameObject);
        }


       
    }

    private void OnTriggerExit2D(Collider2D other)
        //returning to default after rope climbing
    {
        if (other.gameObject.tag=="Rope" && _state==5)
        {
            _state = 2;
            _canMove = true;
            GetComponent<Rigidbody2D>().gravityScale = 1;
            _anim.SetBool("RopeClimb", false);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        //getting on a rope
        if (other.gameObject.tag == "Rope" && !_grounded && _canCollideWithRope)
        {
            Stationary();
            _state = 5;
            _anim.SetBool("RopeClimb", true);
            GetComponent<Rigidbody2D>().MovePosition(new Vector2(other.transform.position.x, transform.position.y));

        }
        //getting on an edge from below
        if (other.gameObject.tag == "edge" && _anim.GetBool("Edging") == false && _grounded == false && ((other.transform.position.x<transform.position.x && transform.localScale.x<0)|| (other.transform.position.x > transform.position.x && transform.localScale.x >0) || _state==5) && Mathf.Abs(_velocityVector.x) >= 0 && Input.GetKey(KeyCode.W))
        {
            
            GetComponent<Transform>().position = new Vector3(other.transform.position.x, other.transform.position.y - (float)1.5, transform.position.z);
            Stationary();
            _anim.SetBool("Edging", true);
            _climbPos = GetComponent<Transform>().position;
            _state = 3;
        }
        //getting on an edge from above
        if(other.gameObject.tag == "edge" && _anim.GetBool("Edging") == false && _grounded == true && ((other.transform.position.x < transform.position.x && transform.localScale.x < 0) || (other.transform.position.x > transform.position.x && transform.localScale.x > 0)) && Input.GetKey(KeyCode.S))
        {
            Flip();
            
            GetComponent<Transform>().position = new Vector3(other.transform.position.x, other.transform.position.y - (float)1.5, transform.position.z);
            Stationary();
            _anim.SetBool("Edging", true);
            _climbPos = GetComponent<Transform>().position;
            _state = 3;
        }
        //setting checkoint
        if (other.gameObject.tag == "CheckPoint")
        {
            Vector3 tempp;
            tempp = other.gameObject.transform.position;
            _checkpoint = tempp;
            DestroyObject(other.gameObject);
            Instantiate(Check, tempp,Quaternion.identity);
        }
        //knockback from broken generator
        if(other.gameObject.tag=="Effect")
        {
            if (other.gameObject.GetComponent<EffectorScript>().Effect==0)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2((transform.position.x - other.transform.position.x)*300, (transform.position.y - other.transform.position.y)*300));
            }
        }

        //getting of rope when hit ground
        if (other.gameObject.tag == "Rope" && _grounded)
        {
            _canMove = true;
            GetComponent<Rigidbody2D>().gravityScale = 1;
            _anim.SetBool("RopeClimb", false);
            _state = 1;



        }
        // finishing the level
        if(other.gameObject.tag=="EndOfLevelTag")
        {
           bool _lockedtest = true;
           int _levelToUnlcok = 0;
           while(_levelToUnlcok < GameProperties.Manager.GetNumberOfLevels() && _lockedtest)
            {
                 _lockedtest=GameProperties.Manager.GetLevelUnlocked(_levelToUnlcok);
                _levelToUnlcok += 1;
            }
            GameProperties.Manager.IncreaseMaxEnergy(1);
            GameProperties.Manager.IncreaseMaxHealth(1);
            GameProperties.Manager.UnlockLevel(_levelToUnlcok);
            GameProperties.Manager.SetLastPlayedCharacter(_character);
            LevelManager.Manager.LoadWorldMap();
        }


    }
    /// <summary>
    /// Flip the player sprite
    /// </summary>
    public void Flip()
    {
        _facingRight = !_facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    /// <summary>
    /// climb up an edge
    /// </summary>
    public void Climb()
    {
        _canMove = true;
        GetComponent<Rigidbody2D>().gravityScale = 1;
        GetComponent<Transform>().position = new Vector3(_climbPos.x+transform.localScale.x/_scale.x * (float)1.4, _climbPos.y+(float)1.4, _climbPos.z);
        _grounded = true;
        _anim.SetBool("Climbing", false);
        _anim.SetBool("Edging", false);


    }
    /// <summary>
    /// Move the combat point for attacking
    /// </summary>
    public void Thrustf()
    {
        CombatPoint.transform.position = new Vector3(CombatPoint.transform.position.x + transform.localScale.x*(float)0.4, CombatPoint.transform.position.y);
    }

    public void Thrustd()
    {
        CombatPoint.transform.position = new Vector3(CombatPoint.transform.position.x, CombatPoint.transform.position.y-(float)0.1);
    }

    public void Thrustu()
    {
        CombatPoint.transform.position = new Vector3(CombatPoint.transform.position.x, CombatPoint.transform.position.y + (float)0.1); 
    }
    /// <summary>
    /// move combat point back after attacking
    /// </summary>
    public void Thrustb()
    {
        CombatPoint.transform.position = new Vector3(CombatPoint.transform.position.x - transform.localScale.x * (float)0.4, CombatPoint.transform.position.y);
    }
    /// <summary>
    /// shoot projetile based on character 
    /// </summary>
    public void Fire()
    {
        DecreaseEnergy(_character);

        GameObject Projectile = (GameObject)Instantiate(this.Projectile, new Vector2(transform.position.x + transform.localScale.x/_scale.x * (float)0.2, transform.position.y + 1.8F), Quaternion.identity);

            Vector3 dir;
            if (_facingRight)
                dir = new Vector3( + 1,0 , 0).normalized;
            else
                dir = new Vector3(- 1, 0, 0).normalized;

            
            Projectile.GetComponent<BulletController>().SetDammage(RangedDamage);
            Projectile.GetComponent<BulletController>().SetDammage(RangedDamage);
        if(_anim.runtimeAnimatorController == CompSci || _anim.runtimeAnimatorController ==TV)
        {
            Projectile.GetComponent<BulletController>().Bullet();
            Projectile.GetComponent<Rigidbody2D>().AddForce(dir * 5, ForceMode2D.Impulse);
        }
        else if (_anim.runtimeAnimatorController == Bio || _anim.runtimeAnimatorController == Art)
            {
                Projectile.GetComponent<BulletController>().Knife();
                Projectile.GetComponent<Rigidbody2D>().AddForce(dir * 3, ForceMode2D.Impulse);
        }
        else if (_anim.runtimeAnimatorController == Phy )
        {
            Projectile.GetComponent<BulletController>().Rocket();
            Projectile.GetComponent<Rigidbody2D>().AddForce(dir * 2, ForceMode2D.Impulse);
        }
        else if (_anim.runtimeAnimatorController == Chem)
        {
            Projectile.GetComponent<BulletController>().Grenade();
            Projectile.GetComponent<Rigidbody2D>().AddForce(dir * 3, ForceMode2D.Impulse);
        }
    }
    /// <summary>
    /// stop attacking and pass it on to combat point
    /// </summary>
    public void StopAttacking()
    {
        CombatPoint.gameObject.GetComponent<CombatTip>().SetAttack(false);
    }
    /// <summary>
    /// take damage = to amount if less than 0 restart.
    /// </summary>
    /// <param name="ammount"> amount to take damage </param>
    public void Takedammage(float ammount)
    {
        if (!_invulnerable)
        {
            _health -= ammount;
            if (HealthBarCanvas)
                UpdateHealthBar();
            _invulnerable = true;
            _alarms[0] = 30;
            if (_health <= 0)
            {
                transform.position = _checkpoint;
                Debug.Log("player dead");
                _health = GameProperties.Manager.PlayerMaxHealth;
                if (HealthBarCanvas)
                    UpdateHealthBar();
            }
        }
    }
    /// <summary>
    /// update the health bar to reflect health
    /// </summary>
    public void UpdateHealthBar()
    {
        if (HealthBarCanvas != null)
        {
            HealthBarCanvas.transform.Find("HealthBar").GetComponent<Image>().fillAmount = (float)_health / (float)GameProperties.Manager.PlayerMaxHealth; ;

        }
    }
    /// <summary>
    /// return the facing 
    /// </summary>
    /// <returns>returns bool true for facing right</returns>
    public int GetFacing()
    {
        if (_facingRight)
            return 1;
        else
            return -1;
    }
    /// <summary>
    /// Switch character 
    /// </summary>
    /// <param name="characterToBe">character to be 0-5</param>
    public void Switch(int characterToBe)
    {
        switch(characterToBe)
        {
            case 0:
                { _anim.runtimeAnimatorController = CompSci;
                    MeleeDamage = 2;
                    RangedDamage = 6;
                }
                break;
            case 1:
                { _anim.runtimeAnimatorController = Bio;
                    MeleeDamage = 3;
                    RangedDamage = 3;
                }
                break;
            case 2:
                { _anim.runtimeAnimatorController = Phy;
                    MeleeDamage = 2;
                    RangedDamage = 15;
                }
                break;
            case 3:
                { _anim.runtimeAnimatorController =Chem;
                    MeleeDamage = 3;
                    RangedDamage = 4;
                }
                break;
            case 4:
                { _anim.runtimeAnimatorController = Art;
                    MeleeDamage = 4;
                    RangedDamage = 3;
                }
                break;
            case 5:
                { _anim.runtimeAnimatorController = TV;
                    MeleeDamage = 1;
                    RangedDamage = 8;
                }
                break;
        }
        CombatPoint.GetComponent<CombatTip>().SetDammage(MeleeDamage);
        _gamePropertiesScript.ActiveCharacter = characterToBe;
        _character = characterToBe;
    }
    /// <summary>
    /// retieve the dammage done
    /// </summary>
    /// <returns></returns>
    public int GetAttack()
    {
        return MeleeDamage;
    }
    /// <summary>
    /// Increase Eneregy by an amount
    /// </summary>
    /// <param name="amount">the ammount of increase energy by</param>
    public void IncreaseEnergy(double amount)
    {
        if (_energy < GameProperties.Manager.PlayerMaxEnergy)
            _energy +=amount;
        if (_energy > GameProperties.Manager.PlayerMaxEnergy)
            _energy = GameProperties.Manager.PlayerMaxEnergy;
        UpdateEnergyBar();
    }
    /// <summary>
    /// Decrease Energy by amount
    /// </summary>
    /// <param name="character"> the amount to dfecrease energy by</param>
    public void DecreaseEnergy(int character)
    {
        _energy -= _energyCost[character];
        UpdateEnergyBar();
    }

    /// <summary>
    /// Update Energy Bar to refelct energy
    /// </summary>
    public void UpdateEnergyBar()
    {
        if (HealthBarCanvas != null)
        {
            HealthBarCanvas.transform.Find("EnergyBar").GetComponent<Image>().fillAmount = (float) _energy / (float)GameProperties.Manager.PlayerMaxEnergy; ;
        }
    }
    /// <summary>
    /// stop the player 
    /// </summary>
    private void Stationary()
    {
        _canMove = false;
        GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
        GetComponent<Rigidbody2D>().gravityScale = 0;
    }
}
