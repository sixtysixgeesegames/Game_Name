﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameProperties : MonoBehaviour
{

    // Singleton instance - can be accessed by: GameProperties.Manager.
    public static GameProperties Manager;


    /// <summary>
    /// Ensures singleton class behaviour. If another GameProperties exists, this GameObject will be destroyed. 
    /// This means GamePropertie can be placed on every scene and will not override the static instance tracking progression.
    /// </summary>
    void Awake()
    {
        if (Manager == null)
        {
            DontDestroyOnLoad(gameObject);

            Manager = this;

            //Remember the character the player is
            Character = 0;

            // A character is represented at each index, true meaning unlocked, false otherwise.
            Characters = new[]
            {
                true, true, true, true, true, true
            };

            // A level is represented at each index, true meaning unlocked, false otherwise.
            Levels = new[]
            {
                true, true, true, false, false, false, false, true, true, false
            };

            // Player's starting currency.
            Currency = 0;

            //Player's starting health
            PlayerMaxHealth = 3;

            //Player's starting energy
            PlayerMaxEnergy = 10;
        }
        else if (Manager != this)
        {
            Destroy(gameObject);
        }
    }

    //Represents what character the player used last.

    public int Character {get;set;}

    /// <summary>
    /// Sets the last played character.
    /// <param name="Character"> No. of Character last played as.</param>
    /// </summary>
    /// 
    public void SetLastPlayedCharacter(int character)
    {
        Character = character;
    }

    /// <summary>
    /// Gets the last played character.
    /// <returns> Returns the int value of the last played character </returns>
    /// </summary>
    /// 
    public int GetLastPlayedCharacter()
    {
        return Character;
    }

    // Represents the whether a character has been unlocked. Character at each index in array. True is unlocked, false is locked.
    public bool[] Characters { get; set; }

    /// <summary>
    /// Checks whether selected character is available.
    /// </summary>
    /// <param name="characterNumber">No. of character being checked, indexed from 0.</param>
    /// <returns>True for available, false otherwise.</returns>
    public bool GetCharacterUnlocked(int characterNumber)
    {
        return Characters[characterNumber];
    }

    /// <summary>
    /// Unlocks the character.
    /// </summary>
    /// <param name="characterNumber">The character number, indexed from 0.</param>
    public void UnlockCharacter(int characterNumber)
    {
        Characters[characterNumber] = true;
    }

    /// <summary>
    /// Locks the character.
    /// </summary>
    /// <param name="characterNumber">The character number, indexed from 0.</param>
    public void LockCharacter(int characterNumber)
    {
        Characters[characterNumber] = false;
    }


    public int ActiveCharacter { get; set; }



    // Levels.

    public bool[] Levels { get; set; }

    /// <summary>
    /// Checks whether level is available.
    /// </summary>
    /// <param name="levelNumber">No. of level being checked, indexed from 0.</param>
    /// <returns>True for available, false otherwsie.</returns>
    public bool GetLevelUnlocked(int levelNumber)
    {
        return Levels[levelNumber];
    }

    /// <summary>
    /// Checks how many levels there is.
    /// </summary>
    /// <returns>Number Of Levels as Int</returns>
    public int GetNumberOfLevels()
    {
        return Levels.Length;
    }

    /// <summary>
    /// Unlocks the level.
    /// </summary>
    /// <param name="levelNumber">The level number, indexed from 0.</param>
    public void UnlockLevel(int levelNumber)
    {
        Levels[levelNumber] = true;
    }

    /// <summary>
    /// Locks the level.
    /// </summary>
    /// <param name="levelNumber">The level number, indexed from 0.</param>
    public void LockLevel(int levelNumber)
    {
        Levels[levelNumber] = false;
    }

    // Active level the player is on.
    public Scene CurrentLevel { get; set; }

    // Used to determine whether pause should be active when level loads. Allows start menu to be displayed at start.
    public bool StartMenuOff { get; set; }

    // Currency

    public int Currency { get; set; }


    /// <summary>
    /// Adds amount of currency to player's total.
    /// </summary>
    /// <param name="amount">The amount.</param>
    /// /// <returns> True if successful, false otherwise. </returns>
    public bool AddCurrency(int amount)
    {
        // Prevent adding negative currency.
        if (amount >= 0)
        {
            // Prevent overflow errors, Currency will not be incremented.
            try
            {
                Currency = checked(Currency + amount);
            }
            catch (OverflowException)
            {
                Currency += 0;
                return false;
            }

            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// Deducts amount of currency to player's total.
    /// </summary>
    /// <param name="amount">The amount.</param>
    /// <returns> True if successful, false otherwise. </returns>
    public bool DeductCurrency(int amount)
    {
        if (Currency - amount >= 0 && amount >= 0)
        {
            Currency -= amount;
            return true;
        }
        return false;
    }

    // Player health and energy.
    public int PlayerMaxHealth { get; set; }

    /// <summary>
    /// PlayerMaxHealth is increased by the amount.
    /// </summary>
    /// <param name="amount">The amount.</param>
    /// <returns> True if successful, false otherwise. </returns>
    public bool IncreaseMaxHealth(int amount)
    {
        // Prevent overflow errors, Energy will not be incremented.
        try
        {
            PlayerMaxHealth = checked(PlayerMaxHealth + amount);
        }
        catch (OverflowException)
        {
            PlayerMaxHealth += 0;
            return false;
        }
        return true;
    }

    public int PlayerMaxEnergy { get; set; }

    /// <summary>
    /// PlayerMaxEnergy is increased by the amount.
    /// </summary>
    /// <param name="amount">The amount.</param>
    public bool IncreaseMaxEnergy(int amount)
    {
        // Prevent overflow errors, Energy will not be incremented.
        try
        {
            PlayerMaxEnergy = checked(PlayerMaxEnergy + amount);
        }
        catch (OverflowException)
        {
            PlayerMaxEnergy += 0;
            return false;
        }
        return true;
    }

    /// <summary>
    /// For use if the singleton instance of gameproperties needs to be reset.
    /// </summary>
    public static void DestroyGamePropertiesSingleton()
    {
        if (Manager != null)
        {
            Destroy(Manager);
            // As Unity seems to override null when objects are destroyed in this way.
            Manager = null;
        }
        
    }
}
