﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Singleton class. Handles loading of levels by loading in level, calling to LoadingScreen manager and updating GameProperties.
/// </summary>
public class LevelManager : MonoBehaviour {

    // Singleton instance - can be accessed by: LevelManager.Manager.
    public static LevelManager Manager;

    // Loading screen canvas is optional.
    public GameObject LoadScreenCanvas;

    private void Awake()
    {
        // Singleton class.
        Manager = this;
    }


    void Start () {

        if (LoadScreenCanvas != null)
        {
            CloseLoadScreen();
        }

        GameProperties.Manager.CurrentLevel = SceneManager.GetActiveScene();
	}

    /// <summary>
    /// Opens the loading screen. This will remain visible while game is paused for loading. 
    /// </summary>
    public void OpenLoadScreen()
    {
        LoadScreenCanvas.SetActive(true);
    }

    /// <summary>
    /// Closes the loading screen.
    /// </summary>
    public void CloseLoadScreen()
    {
        LoadScreenCanvas.SetActive(false);
    }

    /// <summary>
    /// Opens load screen. Loads selected level (in single mode). 
    /// </summary>
    /// <param name="levelName">Name of level to load.</param>
    public void LoadLevel (string levelName)
    {
        OpenLoadScreen();
        // Start menu off so pause isn't active when loading future scenes.
        GameProperties.Manager.StartMenuOff = true;
        SceneManager.LoadScene(levelName, LoadSceneMode.Single);
    }

    /// <summary>
    ///  Opens load screen. Loads World Map level (in single mode).
    /// </summary>
    public void LoadWorldMap()
    {
        OpenLoadScreen();
        // Start menu off so pause isn't active when loading future scenes.
        GameProperties.Manager.StartMenuOff = true;
        SceneManager.LoadScene("World Map", LoadSceneMode.Single);
    }

    /// <summary>
    /// Quit the game.
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }

}
