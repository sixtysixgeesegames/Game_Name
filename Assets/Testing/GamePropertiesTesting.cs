﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using NUnit.Framework.Constraints;

/// <summary>
/// Testing for GameProperties class. 
/// Mostly method calls, but some instantiation into a real scene needed to test MonoBahviours (awake/start).
/// </summary>
public class GamePropertiesTesting {
    
    /// <summary>
    /// Assert characters are correctly reported as locked or unlocked.
    /// </summary>
    [UnityTest]
	public IEnumerator GetCharacterUnlockedTest() {
        
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Characters = new [] { true, true, true, true, true, true};

        for (var i=0; i < GamePropertiesObject.Characters.Length; i++)
        {
	        Assert.AreEqual(true, GamePropertiesObject.GetCharacterUnlocked(i));
	    }

	    GamePropertiesObject.Characters = new[] { false, false, false, false, false, false };

	    for (var i = 0; i < GamePropertiesObject.Characters.Length; i++)
	    {
	        Assert.AreEqual(false, GamePropertiesObject.GetCharacterUnlocked(i));
	    }
	}

    /// <summary>
    /// Assert characters are unlocked.
    /// </summary>
	[UnityTest]
	public IEnumerator UnlockCharacterTest() {

	    GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Characters = new[] { false, false, false, false, false, false };

	    for (var i = 0; i < GamePropertiesObject.Characters.Length; i++)
	    {
            GamePropertiesObject.UnlockCharacter(i);
	        Assert.AreEqual(true, GamePropertiesObject.GetCharacterUnlocked(i));
	    }

	}

    /// <summary>
    /// Assert characters are locked.
    /// </summary>
    [UnityTest]
    public IEnumerator LockCharacterTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Characters = new[] { true, true, true, true, true, true };

        for (var i = 0; i < GamePropertiesObject.Characters.Length; i++)
        {
            GamePropertiesObject.LockCharacter(i);
            Assert.AreEqual(false, GamePropertiesObject.GetCharacterUnlocked(i));
        }

    }

    /// <summary>
    /// Assert levels are correctly reported as locked or unlocked.
    /// </summary>
    [UnityTest]
    public IEnumerator GetLevelUnlockedTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Levels = new[] { true, true, true, true, true, true, true, true, true, true, true, true };

        for (var i = 0; i < GamePropertiesObject.Levels.Length; i++)
        {
            Assert.AreEqual(true, GamePropertiesObject.GetLevelUnlocked(i));
        }

        GamePropertiesObject.Levels = new[] { false, false, false, false, false, false, false, false, false, false, false, false };

        for (var i = 0; i < GamePropertiesObject.Levels.Length; i++)
        {
            Assert.AreEqual(false, GamePropertiesObject.GetLevelUnlocked(i));
        }
    }

    /// <summary>
    /// Assert levels are correctly unlocked.
    /// </summary>
    [UnityTest]
    public IEnumerator UnlockLevelTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Levels = new[] { false, false, false, false, false, false, false, false, false, false, false, false };

        for (var i = 0; i < GamePropertiesObject.Levels.Length; i++)
        {
            GamePropertiesObject.UnlockLevel(i);
            Assert.AreEqual(true, GamePropertiesObject.GetLevelUnlocked(i));
        }
    }

    /// <summary>
    /// Assert levels are locked correctly.
    /// </summary>
    [UnityTest]
    public IEnumerator LockLevelTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Levels = new[] { true, true, true, true, true, true, true, true, true, true, true, true };

        for (var i = 0; i < GamePropertiesObject.Levels.Length; i++)
        {
            GamePropertiesObject.LockLevel(i);
            Assert.AreEqual(false, GamePropertiesObject.GetLevelUnlocked(i));
        }
    }

    /// <summary>
    /// Assert that currency is added, won't overflow, and won't accept negative values.
    /// </summary>
    [UnityTest]
    public IEnumerator AddCurrencyTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Currency = 0;

        GamePropertiesObject.AddCurrency(5);
        Assert.AreEqual(5, GamePropertiesObject.Currency);

        // Doesn't overflow
        GamePropertiesObject.Currency = Int32.MaxValue;
        GamePropertiesObject.AddCurrency(1);
        Assert.AreEqual(Int32.MaxValue, GamePropertiesObject.Currency);

        // Won't accept neagtive values
        GamePropertiesObject.Currency = 0;
        GamePropertiesObject.AddCurrency(-1);
        Assert.AreEqual(0, GamePropertiesObject.Currency);
    }

    /// <summary>
    /// Assert that currency is deducted, won't overflow, and won't accept neagtive values.
    /// </summary>
    [UnityTest]
    public IEnumerator DeductCurrencyTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.Currency = 10;

        GamePropertiesObject.DeductCurrency(5);
        Assert.AreEqual(5, GamePropertiesObject.Currency);

        // Won't go neagtive
        GamePropertiesObject.Currency = 0;
        GamePropertiesObject.DeductCurrency(1);
        Assert.AreEqual(0, GamePropertiesObject.Currency);

        // Won't accept neagtive values
        GamePropertiesObject.Currency = 0;
        GamePropertiesObject.AddCurrency(-1);
        Assert.AreEqual(0, GamePropertiesObject.Currency);

    }

    /// <summary>
    /// Assert that PlayerMaxEnergy is increased and won't overflow.
    /// </summary>
    [UnityTest]
    public IEnumerator IncreaseMaxEnergyTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.PlayerMaxEnergy = 0;

        GamePropertiesObject.IncreaseMaxEnergy(5);
        Assert.AreEqual(5, GamePropertiesObject.PlayerMaxEnergy);

        // Doesn't overflow
        GamePropertiesObject.PlayerMaxEnergy = Int32.MaxValue;
        GamePropertiesObject.IncreaseMaxEnergy(1);
        Assert.AreEqual(Int32.MaxValue, GamePropertiesObject.PlayerMaxEnergy);
    }

    /// <summary>
    /// Assert that PlayerMaxHealth is increased and won't overflow.
    /// </summary>
    [UnityTest]
    public IEnumerator IncreaseMaxHealthTest()
    {
        GameProperties GamePropertiesObject = new GameProperties();

        yield return new WaitForFixedUpdate();

        GamePropertiesObject.PlayerMaxHealth = 0;

        GamePropertiesObject.IncreaseMaxHealth(5);
        Assert.AreEqual(5, GamePropertiesObject.PlayerMaxHealth);

        // Doesn't overflow
        GamePropertiesObject.PlayerMaxHealth = Int32.MaxValue;
        GamePropertiesObject.IncreaseMaxHealth(1);
        Assert.AreEqual(Int32.MaxValue, GamePropertiesObject.PlayerMaxHealth);
    }

    /// <summary>
    /// Ensure utility method to remove the singleton instance does so.
    /// </summary>
    [UnityTest]
    public IEnumerator DestroyGamePropertiesSingletonTest()
    {
        // Add GameProperties Script to scene, as Awake needs to run.
        GameObject GamePropertiesObject = new GameObject();
        GamePropertiesObject.AddComponent<GameProperties>();

        yield return new WaitForFixedUpdate();

        Assert.IsInstanceOf<GameProperties>(GameProperties.Manager);

        GameProperties.DestroyGamePropertiesSingleton();

        Assert.IsNull(GameProperties.Manager);


    }

    /// <summary>
    /// Testing that singleton instance (stored as a static variable in GameProperties) assigned correctly and is not overwritten by new instance.
    /// </summary>
    [UnityTest]
    public IEnumerator IsSingletonInstanceOnAwake()
    {
        GameProperties.DestroyGamePropertiesSingleton();

        // Add GameProperties Script to scene, as Awake needs to run.
        GameObject GamePropertiesObject = new GameObject();
        GamePropertiesObject.AddComponent<GameProperties>();

        yield return new WaitForFixedUpdate();

        Assert.AreSame(GamePropertiesObject.GetComponent<GameProperties>(), GameProperties.Manager);

        // Ensuring new GameProperties does not override the previous singleton instance. 
        GameObject GamePropertiesObject2 = new GameObject();
        GamePropertiesObject2.AddComponent<GameProperties>();

        yield return new WaitForFixedUpdate();

        Assert.AreSame(GamePropertiesObject.GetComponent<GameProperties>(), GameProperties.Manager);

    }

}
