﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

public class WorldMapTesting {


    [SetUp]
    public void SetupTestScene()
    {
        // GameProperties set with some levels unlocked.
        GameObject GamePropertiesObject = new GameObject();
        GamePropertiesObject.AddComponent<GameProperties>();
        GamePropertiesObject.GetComponent<GameProperties>().Levels = new [] { true, true, true, true, true, true, false, false, false, false, false, false};

        // WorldMapCanvas loaded from prefab. 
        UnityEngine.Object WorldMapCanvas =
        AssetDatabase.LoadAssetAtPath("Assets/GameManagementAndUIPrefabs/WorldMapCanvas.prefab",
            typeof(GameObject));

        GameObject WorldMapCanvasFromPrefab = (GameObject)UnityEngine.Object.Instantiate(WorldMapCanvas);
        WorldMapCanvasFromPrefab.name = "WorldMapCanvas";

        GameObject WorldMapObject = new GameObject();
        WorldMapObject.AddComponent<WorldMap>();
        WorldMapObject.GetComponent<WorldMap>().WorldMapCanvas = WorldMapCanvasFromPrefab;

    }

    /// <summary>
    /// Examine whether buttons are interactable to see if levels are available according to levels unlocked.
    /// </summary>
	[UnityTest]
	public IEnumerator LevelsUnlockedCorrectly()
    {
        yield return new WaitForFixedUpdate();

        for (int i = 0; i < 6; i++)
        {
            Assert.IsTrue(GameObject.Find("WorldMapCanvas").transform.Find("ButtonLvl" + i).GetComponent<Button>().IsInteractable());
        }
        for (int i = 6; i < 12; i++)
        {
            Assert.IsFalse(GameObject.Find("WorldMapCanvas").transform.Find("ButtonLvl" + i).GetComponent<Button>().IsInteractable());
        }

        yield return new WaitForFixedUpdate();
	}
}
