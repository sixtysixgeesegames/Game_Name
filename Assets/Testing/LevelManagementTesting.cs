﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Tests whether levels are correctly loaded and whether loading screen opens when levels are loaded.
/// </summary>
public class LevelManagementTesting {

    [SetUp]
    public void SetupTestScene()
    {
        GameObject GamePropertiesObject = new GameObject();
        GamePropertiesObject.name = "GameProp";
        GamePropertiesObject.AddComponent<GameProperties>();

        GameObject LoadingScreen = new GameObject();
        LoadingScreen.name = "LoadScrn";

        GameObject LevelManagementObject = new GameObject();
        LevelManagementObject.name = "LevelMan";
        LevelManagementObject.AddComponent<LevelManager>();
        LevelManagementObject.GetComponent<LevelManager>().LoadScreenCanvas = LoadingScreen;
    }

    [TearDown]
    public void ResetTestScene()
    {
        foreach (GameObject o in UnityEngine.Object.FindObjectsOfType<GameObject>())
        {
            GameObject.Destroy(o);
        }
    }

    /// <summary>
    /// Ensure Manager is singleton class. 
    /// </summary>
	[UnityTest]
	public IEnumerator LevelManagementIsSingletonClass()
	{
	    yield return new WaitForFixedUpdate();
	    Assert.AreSame(GameObject.Find("LevelMan").GetComponent<LevelManager>(), LevelManager.Manager);
	}

    /// <summary>
    /// Ensure current level is set according to the level that is opened.
    /// </summary>
    /// <returns></returns>
    [UnityTest]
    public IEnumerator CurrentLevelSetInGamePropertiesTest()
    {
        yield return new WaitForFixedUpdate();

        Assert.True(GameProperties.Manager.CurrentLevel.name == SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Ensure load screen opens.
    /// </summary>
    [UnityTest]
    public IEnumerator OpenLoadScreenTest()
    {
        yield return new WaitForFixedUpdate();

        LevelManager.Manager.LoadScreenCanvas.SetActive(false);
        yield return new WaitForFixedUpdate();

        LevelManager.Manager.OpenLoadScreen();
        yield return new WaitForFixedUpdate();

        Assert.True(LevelManager.Manager.LoadScreenCanvas.activeSelf);
    }

    /// <summary>
    /// Ensure load screen closes.
    /// </summary>
    [UnityTest]
    public IEnumerator CloseLoadScreenTest()
    {
        yield return new WaitForFixedUpdate();

        LevelManager.Manager.LoadScreenCanvas.SetActive(true);
        yield return new WaitForFixedUpdate();

        LevelManager.Manager.CloseLoadScreen();
        yield return new WaitForFixedUpdate();
        Assert.False(LevelManager.Manager.LoadScreenCanvas.activeSelf);

        yield return new WaitForFixedUpdate();
    }

    /// <summary>
    /// Ensure levels are correctly loaded by loading a test scene in the build.
    /// </summary>
    [UnityTest]
    public IEnumerator LoadLevelTest()
    {
        yield return new WaitForFixedUpdate();

        LevelManager.Manager.LoadLevel("UnitTestScene");
        yield return new WaitForFixedUpdate();

        Assert.True(SceneManager.GetActiveScene().name == "UnitTestScene");

        yield return new WaitForFixedUpdate();
    }

    /// <summary>
    /// Ensure world map is loaded.
    /// </summary>
    [UnityTest]
    public IEnumerator LoadWorldMapTest()
    {
        yield return new WaitForFixedUpdate();

        LevelManager.Manager.LoadLevel("World Map");
        yield return new WaitForFixedUpdate();

        Debug.Log(SceneManager.GetActiveScene().name);
        Assert.True(SceneManager.GetActiveScene().name == "World Map");

        yield return new WaitForFixedUpdate();
    }
}
