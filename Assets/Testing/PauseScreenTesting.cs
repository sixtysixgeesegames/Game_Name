﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using NUnit.Framework.Constraints;
using UnityEngine.SceneManagement;

/// <summary>
/// Tests opening and closing of the pause menu, and whether keyboard input is registered.
/// </summary>
public class PauseScreenTesting {


    [SetUp]
	public void SetupTestScene() {

        // Setup pause canvas object.
	    GameObject PauseCanvas = new GameObject();
	    PauseCanvas.name = "PasueScrn";

        // Setup GameProperites object and ensure pause is not active on start.
        GameObject GamePropertiesObject = new GameObject();
        GamePropertiesObject.AddComponent<GameProperties>().StartMenuOff = true;

        // Setup PauseManager object.
	    GameObject PauseScreenManagerObject = new GameObject();
	    PauseScreenManagerObject.name = "PauseMan";
	    PauseScreenManagerObject.AddComponent<PauseManager>().PauseScreenCanvas = PauseCanvas;

	}

    [TearDown]
    public void ResetTestScene()
    {
        foreach (GameObject o in UnityEngine.Object.FindObjectsOfType<GameObject>())
        {
            GameObject.Destroy(o);
        }
    }

    /// <summary>
    /// Ensures PauseScreenManager is singleton class.
    /// </summary>
    /// <returns></returns>
    [UnityTest]
    public IEnumerator PauseManagerIsSingeltonClass()
    {
        yield return new WaitForFixedUpdate();
        Assert.AreSame(GameObject.Find("PauseMan").GetComponent<PauseManager>(), PauseManager.Manager);
    }

    /// <summary>
    /// Ensures screen open and closes correctly. 
    /// </summary>
    [UnityTest]
    public IEnumerator OpenAndCloseTest()
    {
        yield return new WaitForFixedUpdate();
        
        PauseManager.Manager.PauseScreenCanvas.SetActive(false);
        yield return new WaitForFixedUpdate();

        PauseManager.Manager.Open();
        Assert.AreEqual(Time.timeScale, 0.0F);
        // reset time or test will freeze. 
        Time.timeScale = 1.0F;
        yield return new WaitForFixedUpdate();

        Assert.IsTrue(PauseManager.Manager.Active);
        Assert.IsTrue(PauseManager.Manager.PauseScreenCanvas.activeSelf);

        Time.timeScale = 0.5F;
        PauseManager.Manager.Close();
        Assert.AreEqual(Time.timeScale, 1.0F);
        yield return new WaitForFixedUpdate();

        Assert.IsFalse(PauseManager.Manager.Active);
        Assert.IsFalse(PauseManager.Manager.PauseScreenCanvas.activeSelf);
        
    }
    /// <summary>
    /// Test will pass if escape key is pressed.
    /// </summary>
    [UnityTest]
    public IEnumerator ScanForOpenCloseTestWithEscPress()
    {
        Debug.Log("Press esc key to test menu opening");
        yield return new WaitForFixedUpdate();

        // wait until user presses esc. Wait for seconds rather than update as time is paused. 
        yield return new WaitUntil(() => Input.anyKeyDown);
        yield return new WaitForSecondsRealtime(0.1F);
        Assert.IsTrue(PauseManager.Manager.Active);

        // So time resumes as normal
        PauseManager.Manager.Close();
        yield return new WaitForFixedUpdate();
    }

}
