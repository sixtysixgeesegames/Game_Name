﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Boo.Lang.Environments;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.UI;

/// <summary>
/// Tests functionality of Character Select menu, including opening/closing and whether locked characters are greyed out and won't be switched to. 
/// </summary>
public class CharSelectManagerTesting
{
    [SetUp]
    public void SetupTestScene()
    {
        // Player setup in scene.

        GameObject PlayerObject = new GameObject();
        PlayerObject.name = "Player";

        // PauseManager setup in scene.

        GameObject PauseManagerObject = new GameObject();
        PauseManagerObject.AddComponent<PauseManager>();

        // GameProperties setup in scene.

        GameObject GamePropertiesObject = new GameObject();
        GamePropertiesObject.AddComponent<GameProperties>();
        GamePropertiesObject.GetComponent<GameProperties>().Characters = new[] { true, true, true, false, false, false };

        // Load our canvas as it has the children required.

        UnityEngine.Object CharSelectCanvasPrefab =
            AssetDatabase.LoadAssetAtPath("Assets/GameManagementAndUIPrefabs/CharSelectCanvas.prefab",
                typeof(GameObject));

        GameObject CharSelectCanvasFromPrefab = (GameObject)UnityEngine.Object.Instantiate(CharSelectCanvasPrefab);
        CharSelectCanvasFromPrefab.name = "CharSelectCanvas";

        // CharSelectManager setup in scene.

        GameObject CharSelectManagerObject = new GameObject();
        CharSelectManagerObject.name = "CharSelectScrn";
        CharSelectManagerObject.AddComponent<CharSelectManager>();
        CharSelectManagerObject.GetComponent<CharSelectManager>().CharSelectCanvas = CharSelectCanvasFromPrefab;
        CharSelectManagerObject.GetComponent<CharSelectManager>().PlayerCharacter = PlayerObject;

    }

    [TearDown]
    public void ResetTestScene()
    {
        foreach (GameObject o in UnityEngine.Object.FindObjectsOfType<GameObject>())
        {
            GameObject.Destroy(o);
        }
    }

    /// <summary>
    /// Ensure there is a single static instance in the class.
    /// </summary>
    [UnityTest]
    public IEnumerator CharSelectScreenIsSingelton()
    {
        yield return new WaitForFixedUpdate();
        Assert.AreSame(GameObject.Find("CharSelectScrn").GetComponent<CharSelectManager>(), CharSelectManager.Manager);
    }

    /// <summary>
    /// If there is no canvas or player in the scene, the script should be disabled.
    /// </summary>
    [UnityTest]
    public IEnumerator CharSelectScreenDisabledIfNoPlayerOrCanvas()
    {
        // Assert that is active when there is a charselect canvas and player.
        yield return new WaitForFixedUpdate();
        Assert.IsTrue(CharSelectManager.Manager.enabled);

        // Create blank scene where there is no Player or character switching canvas.   
        ResetTestScene();
        yield return new WaitForFixedUpdate();

        GameObject CharSelectScreenObject = new GameObject();
        CharSelectScreenObject.AddComponent<CharSelectManager>();
        yield return new WaitForFixedUpdate();
        Assert.IsFalse(CharSelectManager.Manager.enabled);

    }

    /// <summary>
    /// Ensures canvas is enabled and disabled appropriately. Tested together to ensure correct behaviour.
    /// </summary>
    [UnityTest]
    public IEnumerator OpenCloseTest()
    {
        yield return new WaitForFixedUpdate();

        CharSelectManager.Manager.CharSelectCanvas.SetActive(false);
        yield return new WaitForFixedUpdate();

        CharSelectManager.Manager.Open();
        yield return new WaitForFixedUpdate();

        Assert.IsTrue(CharSelectManager.Manager.Active);
        Assert.IsTrue(CharSelectManager.Manager.CharSelectCanvas.activeSelf);

        CharSelectManager.Manager.Close();
        yield return new WaitForFixedUpdate();

        Assert.IsFalse(CharSelectManager.Manager.Active);
        Assert.IsFalse(CharSelectManager.Manager.CharSelectCanvas.activeSelf);
    }

    /// <summary>
    /// Test will pass if escape key is pressed.
    /// </summary>
    [UnityTest]
    public IEnumerator ScanForOpenCloseTestWithTabPress()
    {
        Debug.Log("Press tab key to test menu opening");
        yield return new WaitForFixedUpdate();
        // wait until user presses tab. 
        yield return new WaitUntil(() => Input.anyKeyDown);
        yield return new WaitForFixedUpdate();
        Assert.IsTrue(CharSelectManager.Manager.Active);
        // So time resumes as normal
        CharSelectManager.Manager.Close();
        yield return new WaitForFixedUpdate();
    }

    /// <summary>
    /// Verify button are blacked out and do nothing on click when associated character is locked.
    /// </summary>
    [UnityTest]
    public IEnumerator ButtonsSetupCorrectly()
    {
        yield return new WaitForFixedUpdate();
        // Button colouring and assert CallSwitchFunction return false when character locked.
        Debug.Log(CharSelectManager.Manager.CharSelectCanvas.transform.Find("CharSelect0").GetComponent<Image>().color);
        for (var i = 0; i < 3; i++)
        {
            Assert.AreEqual(
                CharSelectManager.Manager.CharSelectCanvas.transform.Find("CharSelect" + i).GetComponent<Image>().color,
                new Color(1.0f, 1.0f, 1.0f, 1.0f));
        }

        for (var i = 3; i < 6; i++)
        {
            Assert.AreEqual(
                CharSelectManager.Manager.CharSelectCanvas.transform.Find("CharSelect" + i).GetComponent<Image>().color,
                new Color(0.0f, 0.0f, 0.0f, 1.0f));
            Assert.IsFalse(CharSelectManager.Manager.CallSwitchFucntion(i));
        }
    }
}
